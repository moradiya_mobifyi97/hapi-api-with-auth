const service = require('./service')
const authtest = require('hapi-auth-basic')

exports.register = async function(){
    await server.register(require('hapi-auth-basic'));

    server.auth.strategy('simple', 'basic', { 
        validate:service.validate
    });
};

exports.pkg = {
    name: 'auth'
}