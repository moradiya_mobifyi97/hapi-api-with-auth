const Bcrypt = require('bcrypt');
const Hapi = require('hapi');
var User =  require('../models/user');
 

module.exports.validate = async (request, email, password, h) => {
    const users = await User.findOne({email:email});
    const user = users[email];
    console.log("useruser", user)
    if (!user) {
        return { credentials: null, isValid: false };
    }
 
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, email: user.email };
 
    return { isValid, credentials };
};
