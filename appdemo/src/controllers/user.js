var User =  require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.create = (req, h) => {
    const userData = {
        name:req.payload.name,
        email:req.payload.email,
        password:req.payload.password
    }

    return User.findOne({email:req.payload.email})
    .then((user) => {
        if(!user){
            bcrypt.hash(req.payload.password,10,(err, hash) => {
                userData.password = hash;
                return User.create(userData)
                .then((userResult) => {

                    return { message: "user registered", userResult: userResult };
               
                 }).catch((err) => {
                    return { err: err };
                });
            })
            return {status:userData.email = 'registered!'}
        }
        return {message:"registerd!"}
    })
    .catch((err) => {
        return {err:err}
    })
}

exports.login ={
    
}