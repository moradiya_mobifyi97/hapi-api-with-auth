var Dog =  require('../models/dog');

// list
exports.list = (req, h) => {
  return Dog.find({}).exec().then((dog) => {
    return { dogs: dog };
  }).catch((err) => {
    return { err: err };
  });
}

// Get by id
exports.get = (req, h) => {

  return Dog.findById(req.params.id).exec().then((dog) => {
    if(!dog) return { message: 'Dog not Found' };
    return { dog: dog };
  }).catch((err) => {

    return { err: err };

  });
}


// post
exports.create = (req, h) => {

  const dogData = {
    name: req.payload.name,
    breed: req.payload.breed,
    age: req.payload.age,
  };

  return Dog.create(dogData).then((dog) => {

     return { message: "Dog created successfully", dog: dog };

  }).catch((err) => {

    return { err: err };

  });
}

 // Update Dog by ID
 
exports.update = (req, h) => {
  console.log('result');
  return Dog.findById(req.params.id).exec().then((dog) => {
   
    if (!dog) return { err: 'Dog not found' };

    dog.name = req.payload.name;
    dog.breed = req.payload.breed;
    dog.age = req.payload.age;
    
    dog.save();

  }).then((result) => {
    
      return { message: "Dog data updated successfully", result:result };

  }).catch((err) => {

      return { err: err };

  });
}


 // Delete Dog by ID

exports.remove = (req, h) => {

  return Dog.findById(req.params.id).exec(function (err, dog) {

    if (err) return { dberror: err };
    if (!dog) return { message: 'Dog not found' };

    dog.remove(function (err) {
      if (err) return { dberror: err };

      return { message: "Dog data delete successfully", success: true };
    });
  });
}