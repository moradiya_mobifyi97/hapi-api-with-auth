const Path = require('path');

module.exports = {
  server: {
    port: 8000
  },
  register: {
    plugins: [
      {
        plugin: require('./routes/users'),
        routes: {
          prefix: '/users'
        }
      }
    ]
  }
};