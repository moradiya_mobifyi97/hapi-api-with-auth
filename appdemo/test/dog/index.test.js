const server = require('../../server');
var it = test;

describe("the plugin", () => {
	const uut = require('../../src/controllers/dog');
	describe('when registered', () => {
        beforeEach(() => {
            server = {
                route:jest.fn()
            };
            uut.register(server);
        })

        describe('the first route', () => {
            beforeEach(() =>{
                route = server.route.mock.calls[1][0];
            })
            it('has a method  of "POST"', () =>{
                expect(route.method).toEqual('POST');
            })
        })
    })
})
