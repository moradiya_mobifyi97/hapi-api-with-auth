'use strict';

const Hapi = require('hapi');
const mongoose = require('mongoose');
const DogController =  require('./src/controllers/dog');
const UserController =  require('./src/controllers/user');
const MongoDBUrl = 'mongodb://localhost:27017/dogapi';
const auth = require('./src/auth')

const server = new Hapi.Server({
  port: 3000,
  host: 'localhost'
});

server.route({
  method: 'POST',
  path: '/users',
  handler: UserController.create
});

server.route({
  method: 'GET',
  path: '/dogs',
  config:{
    auth:"simple"
  },
  handler: DogController.list
});

server.route({
  method: 'GET',
  path: '/dogs/{id}',
  handler: DogController.get
});
server.route({
  method: 'POST',
  path: '/dogs',
  handler: DogController.create
});

server.route({
  method: 'PUT',
  path: '/dogs/{id}',
  handler: DogController.update
});

server.route({
  method: 'DELETE',
  path: '/dogs/{id}',
  handler: DogController.remove
});

(async () => {
  try {  
    await server.start();
    // Once started, connect to Mongo through Mongoose
    mongoose.connect(MongoDBUrl,
      {useNewUrlParser:true})
      .then(() => { 
        console.log(`Connected to Mongo server`) 
      }, err => { console.log(err) });
    console.log(`Server running at: ${server.info.uri}`);
  }
  catch (err) {  
    console.log(err)
  }
})();